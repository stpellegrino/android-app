package com.example.batataapp;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;



import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.support.v4.app.NavUtils;


public class DownloadImage extends Activity {
	public String error = "" ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_download_image);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Button download= (Button) findViewById(R.id.download); 
	       download.setOnClickListener(new OnClickListener()
	       {
			@Override
			public void onClick(View arg0) 
			{
				new AddWebService().execute("empty","mypicfromandroid") ;
				
			}
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.download_image, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class AddWebService extends AsyncTask<String, Void, byte[]> {

		@SuppressWarnings("deprecation")
		@Override
		protected byte[] doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			 String NAMESPACE ="http://showee2/" ;
			 String URL = "http://showee2-showee.rhcloud.com/filedownload" ;
			 String ADD_METHOD_NAME = "download" ;
			String ADD_SOAP_ACTION = "http://showee2/download" ;
		
					
			SoapObject request = new SoapObject(NAMESPACE, ADD_METHOD_NAME) ;
			
		
			
			request.addProperty("arg0","mypicfromandroid") ;
			//	request.addProperty("arg1",(byte[]) Serializer.serialize(params[1])) ;
			//	ByteArrayOutputStream out = new ByteArrayOutputStream();
				
			request.addProperty("arg1", "mypicfromandroid") ;
		
			
		
			 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		//	 new MarshalBase64().register(envelope);
			 envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL) ;
			try {
				androidHttpTransport.call(ADD_SOAP_ACTION, envelope) ;
				//return (SoapPrimitive) envelope.getResponse() ;
			//SoapObject result = (SoapObject)envelope.bodyIn;
				
	                
					//Text Email= (Text) findViewById(R.id.EmailSignUp); // this 2 lines here will crash the code
					//Email.setTextContent("not null") ; // keep them for debugging trials peeps
					
				//return (SoapPrimitive)envelope.getResponse ();
				
				return Base64.decode(envelope.getResponse().toString(), 0); 
			}
			catch(Exception e) {
				
			error =  e.getMessage();
			//	e.printStackTrace();
			}
			return null;
			
		}
		
		@Override
		protected void onPostExecute(byte[] data) {
			
		//	Bitmap bitmap = BitmapFactory.decodeByteArray(data ,0,data.length);
		//	String s = new String(data,(Charset.forName("UTF-8"))) ;
			
			
			ImageView imageView =  (ImageView) findViewById(R.id.imageView1) ;
		//	imageView.setImageBitmap(StringToBitMap(s)) ;
			InputStream is = new ByteArrayInputStream(data);
			Bitmap bmp = BitmapFactory.decodeStream(is);
		
			imageView.setImageBitmap(bmp) ;
		}
		
		protected void onProgressUpdate() {
			
		}
		
		
		
	}
	
	 public Bitmap StringToBitMap(String encodedString){
	     try{
	       byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
	       Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
	       return bitmap;
	     }catch(Exception e){
	       e.getMessage();
	       return null;
	     }
	      }
	
}
