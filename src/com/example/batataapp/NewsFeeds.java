package com.example.batataapp;

import java.sql.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.media.Image;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.os.Build;
import android.R.*;

public class NewsFeeds extends Activity 
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_news_feeds);
		
		//we should look at the actions of all the user's following people and create action for each
		// load all the user's followers 
		int followers=10; 
	
		/*LinearLayout ll=(LinearLayout) findViewById(R.id.main);
		/*LinearLayout main=new LinearLayout(this);

		LinearLayout.LayoutParams mainParam=new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); 
		LinearLayout layout=new LinearLayout(this); 
		LinearLayout.LayoutParams layoutLinearLayout1=new LinearLayout.LayoutParams(150, 200); // set width and height of the layout of followers 
		layoutLinearLayout1.setMargins(110, 140, 0, 0);
		layout.setOrientation(LinearLayout.VERTICAL); 
		layout.setLayoutParams(layoutLinearLayout1);
		
		ScrollView scroll=new ScrollView(this);
		LinearLayout.LayoutParams layoutscroll=new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); 
		scroll.setLayoutParams(layoutscroll);
		TableLayout table= new TableLayout(this);
		LinearLayout.LayoutParams layoutTable=new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); 
		table.setLayoutParams(layoutTable);*/
		
		
		
		TableLayout tl=(TableLayout) findViewById(R.id.table); 
		LinearLayout.LayoutParams layoutImage = new LinearLayout.LayoutParams(0,LayoutParams.MATCH_PARENT); //the image layout 50.40
		LinearLayout.LayoutParams layoutText = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT); // the text layout
		layoutText.weight=6; 
		layoutImage.weight=4; 
		TableRow.LayoutParams layoutRow = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT); // the row layout 
		layoutText.gravity=Gravity.CENTER; 
		 
		for(int i=0; i<followers; i++)
		{ 
			// row stuff!! 
			TableRow row= new TableRow(this); 
			row.setLayoutParams(layoutRow);
			// image stuff!! 
			ImageView fpp=new ImageView(this); //follower profile picture
			fpp.setScaleType(ScaleType.CENTER_CROP);
			fpp.setImageDrawable(getWallpaper());
			
			
			// fpp.setImageAlpha(R.drawable.ic_launcher); // set the image from the web
			fpp.setLayoutParams(layoutImage);
			row.addView(fpp);

			// text stuff! 
			TextView text=new TextView(this); 
			text.setText("Follower"+i); 
			text.setLayoutParams(layoutText);//set the name of the follower from the web
			text.setPadding(10, 0, 0, 0);

			// add the new stuff! 
			row.addView(text);
			tl.addView(row);
		}
		
	}
		


		
		/*if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}*/
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news_feeds, menu);
		return true;
	}
	//method that creates the actions that will be listed
	/*
	 * username
	 * date
	 * image
	 * likes 
	 * comments
	 */
	public void createAction(String username, int id, ImageView image, Date date, int likes)
	{ 
		TableLayout layout=new TableLayout(this); 
		TableRow usernameRow=new TableRow(this); 
		TextView name=new TextView(this); 
		name.setText(username);
		//set the layout later
		usernameRow.addView(name);
		layout.addView(usernameRow);
		//date
		TableRow row2=new TableRow(this); 
		TextView d=new TextView(this);
		d.setText((CharSequence) date);
		row2.addView(d);
		layout.addView(row2);
		//Image
		TableRow row3=new TableRow(this);
		ImageView i=new ImageView(this); 
		/*
		 * i.setImageBitmap(BitmapFactory.decodeFile()));
		 * should set the value of the image to i here, check this later
		 */
		row3.addView(i);
		layout.addView(row3); 
		//Likes and comments
		TableRow row4=new TableRow(this);
		TextView LikesText=new TextView(this);
		LikesText.setText("Likes");
        LikesText.setPadding(20, 20, 20, 20); //check the values later
        row4.addView(LikesText);
        TextView LikesNumber= new TextView(this);
        LikesNumber.setText(""+likes);
        LikesNumber.setPadding(20, 20, 20, 20);
        row4.addView(LikesNumber);
        //comments
        /*check what to do with comments later
         * what datatype are they stored in? 
         * 
         */

        // add row 4 to layout 
        layout.addView(row4);

		
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	/*public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_news_feeds,
					container, false);
			return rootView;
		}
	}*/

}
