package com.example.batataapp;
 
import java.io.StringWriter;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Text;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Build;
import android.preference.PreferenceManager;
import android.view.View.OnClickListener;

public class SignUp extends Activity 
{
	private static String error ;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_sign_up);
		
		Button submit = (Button) findViewById(R.id.SubmitSignUp) ;
		
		submit.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				EditText text = (EditText)findViewById(R.id.EmailSignUp);
				String email = text.getText().toString();
				text = (EditText)findViewById(R.id.NameSignUp);
				String firstname = text.getText().toString();
				text = (EditText)findViewById(R.id.LastNameSignUp);
				String famname = text.getText().toString();
				text = (EditText)findViewById(R.id.signupUsername);
				String username = text.getText().toString();
				String  name = firstname + " " + famname ;
				text = (EditText)findViewById(R.id.signupPassword);
				String password = text.getText().toString();
				
				// saving username and password
				SharedPreferences sp = getSharedPreferences("account", Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = sp.edit();
				editor.putString("username", username);
				editor.putString("password", password);
				editor.commit();
				
				
			new AddWebService().execute(name,username,password, email) ;
		
			}
		});
	
		/*Text Name= (Text) findViewById(R.id.NameSignUp);
		Text Last= (Text)findViewById(R.id.LastNameSignUp);
		Text Email= (Text) findViewById(R.id.EmailSignUp); 
		Button signUp= (Button) findViewById(R.id.Submit); 
		*/
		/*
		if (savedInstanceState == null)
		{
			getFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	/**
	 * A placeholder fragment containing a simple view.
	 */
	/*public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_sign_up,
					container, false);
			return rootView;
		}
	}*/

private class AddWebService extends AsyncTask<String, Void, SoapPrimitive> {

	@SuppressWarnings("deprecation")
	@Override
	protected SoapPrimitive doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		 String NAMESPACE ="http://showee2/" ;
		 String URL = "http://showee2-showee.rhcloud.com/user?wsdl" ;
		 String ADD_METHOD_NAME = "register" ;
		String ADD_SOAP_ACTION = "http://showee2/register" ;
	
				
		SoapObject request = new SoapObject(NAMESPACE, ADD_METHOD_NAME) ;
		
	
		
		request.addProperty("arg0",params[0]) ;
		request.addProperty("arg1",params[1]) ;
		request.addProperty("arg2", params[2]) ;
		request.addProperty("arg3", params[3]) ;
		 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		 
		 envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL) ;
		try {
			androidHttpTransport.call(ADD_SOAP_ACTION, envelope) ;
			//return (SoapPrimitive) envelope.getResponse() ;
			SoapObject result = (SoapObject)envelope.bodyIn;
			
                
				//Text Email= (Text) findViewById(R.id.EmailSignUp); // this 2 lines here will crash the code
				//Email.setTextContent("not null") ; // keep them for debugging trials peeps
				
			return ( SoapPrimitive ) envelope . getResponse ();
		}
		catch(Exception e) {
			
			error =  e.getMessage();
		//	e.printStackTrace();
		}
		return null;
		
	}
	
	protected void onPostExecute(SoapPrimitive result) {
		// text.setText(result.toString()) ;
		EditText text2=(EditText)findViewById(R.id.signupUsername) ;
	text2.setText("reached here") ;
	
	EditText text3=(EditText)findViewById(R.id.signupPassword) ;
	text3.setText(error) ;
	
	}
	
	protected void onProgressUpdate() {
		
	}
	
	
	
}
}
