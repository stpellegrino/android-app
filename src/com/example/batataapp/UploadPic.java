package com.example.batataapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class UploadPic extends Activity implements OnClickListener
{
	public String error = "" ;
	public String error2 = "" ;
	public static int LoadImageOnresults=1; 
	public ImageView image; 


	private String title;
	private int sourceWidth, currentWidth;
	private int sourceHeight, currentHeight;
	private Bitmap sourceImage;
	private Canvas sourceCanvas;        
	private Bitmap currentImage;
	private Canvas currentCanvas;   
	private Paint currentPaint; 
	 
	public String username = "" ;
	public int mediaMaxID = 0 ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_upload_pic);
		Button upload=(Button) findViewById(R.id.UploadPictureButton);
		image=(ImageView) findViewById(R.id.ImageToUpload);
		
		Button upload2=(Button) findViewById(R.id.upload2);
		//intent to start the gallery search
		
		
		SharedPreferences sp = getSharedPreferences("account", Activity.MODE_PRIVATE);
		 username= sp.getString("username", null);
		 
		 
		upload2.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0)
			{

				EditText text3=(EditText)findViewById(R.id.error) ;
				text3.setText("clicked button upload2 :)") ;		
				String un ;
				SharedPreferences sp = getSharedPreferences("account", Activity.MODE_PRIVATE);
				 un= sp.getString("username", null);
				
				 ByteArrayOutputStream stream = new ByteArrayOutputStream();
				 image.buildDrawingCache();
				 Bitmap mImageBitmap = image.getDrawingCache();
				 mImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				 byte[] byteArray = stream.toByteArray();
				 ByteArrayOutputStream os=new ByteArrayOutputStream(); 
				    mImageBitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, 
				(OutputStream) os); 
			
				    new getMaxMediaID().execute() ;
				    
			new AddWebService().execute(username, byteArray ,(mediaMaxID+1)+"") ;
			}
		});
		
		
		final Intent intent3 = new Intent(this, PhotoIntentActivity.class);
		
	
	
		final Intent intent=new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); 
		upload.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0)
			{
				startActivityForResult(intent, LoadImageOnresults);
			}
		});
		
		Button camera=(Button) findViewById(R.id.camera);
		//intent to start the gallery search
		
		camera.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0)
			{
				
				startActivity(intent3);
			}
		});

		/*if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}*/
	}
	@Override
	 public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
        if (resultCode == RESULT_OK && requestCode == LoadImageOnresults && data!=null) 
        {
        	Uri pickedImage=data.getData(); 
        	String[] filePath={ MediaStore.Images.Media.DATA};
        	Cursor cursor=getContentResolver().query(pickedImage, filePath, null, null, null); 
        	cursor.moveToFirst(); 
        	
        	String imagePath =cursor.getString(cursor.getColumnIndex(filePath[0]));
        	image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        	cursor.close(); 
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.upload_pic, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onClick(View arg0)
	{
		
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	/*
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_upload_pic,
					container, false);
			return rootView;
		}
	} */
	private class AddWebService extends AsyncTask<Object, Void, SoapPrimitive> {

		@SuppressWarnings("deprecation")
		@Override
		protected SoapPrimitive doInBackground(Object... params) {
			// TODO Auto-generated method stub
			
			 String NAMESPACE ="http://showee2/" ;
			 String URL = "http://showee2-showee.rhcloud.com/user?wsdl" ;
			 String ADD_METHOD_NAME = "addImage" ;
			String ADD_SOAP_ACTION = "http://showee2/addImage" ;
		
					
			SoapObject request = new SoapObject(NAMESPACE, ADD_METHOD_NAME) ;
			
		
			
			request.addProperty("arg0",(String)params[0]) ;
			//	request.addProperty("arg1",(byte[]) Serializer.serialize(params[1])) ;
			//	ByteArrayOutputStream out = new ByteArrayOutputStream();
				
			request.addProperty("arg1", (byte[])params[1]) ;
			request.addProperty("arg2", (String)params[2]) ;
			
		
			 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			 new MarshalBase64().register(envelope);
			 envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL) ;
			try {
				androidHttpTransport.call(ADD_SOAP_ACTION, envelope) ;
				//return (SoapPrimitive) envelope.getResponse() ;
			//	SoapObject result = (SoapObject)envelope.bodyIn;
				
	                
					//Text Email= (Text) findViewById(R.id.EmailSignUp); // this 2 lines here will crash the code
					//Email.setTextContent("not null") ; // keep them for debugging trials peeps
					
		//		return (SoapPrimitive)envelope.getResponse ();

			//	new getMaxMediaID().execute() ;
				
			}
			catch(Exception e) {
				
			error =  e.getMessage();
			//	e.printStackTrace();
			}
			return null;
			
		}
		
		@Override
		protected void onPostExecute(SoapPrimitive result) {
	
			
			EditText text3=(EditText)findViewById(R.id.error) ;
			text3.setText(error) ;
		}
		
		protected void onProgressUpdate() {
			
		}
		
		
		
	}
	private class getMaxMediaID extends AsyncTask<Object, Void, SoapPrimitive> {

		@SuppressWarnings("deprecation")
		@Override
		protected SoapPrimitive doInBackground(Object... params) {
			// TODO Auto-generated method stub
			
			 String NAMESPACE ="http://showee2/" ;
			 String URL = "http://showee2-showee.rhcloud.com/user?wsdl" ;
			 String ADD_METHOD_NAME = "getMaxMediaID" ;
			String ADD_SOAP_ACTION = "http://showee2/getMaxMediaID" ;
		
					
			SoapObject request = new SoapObject(NAMESPACE, ADD_METHOD_NAME) ;
			
		

			
		
			 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			 new MarshalBase64().register(envelope);
			 envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL) ;
			try {
				androidHttpTransport.call(ADD_SOAP_ACTION, envelope) ;
				//return (SoapPrimitive) envelope.getResponse() ;
			//	SoapObject resultSoap = (SoapObject)envelope.bodyIn;
				
	                
					//Text Email= (Text) findViewById(R.id.EmailSignUp); // this 2 lines here will crash the code
					//Email.setTextContent("not null") ; // keep them for debugging trials peeps
					
			return  (SoapPrimitive) envelope.getResponse() ;
			}
			catch(Exception e) {
				
			error =  e.getMessage();
			//	e.printStackTrace();
			}
			return null;
			
		}
		
		protected void onPostExecute(SoapPrimitive s) {
			
			
			
			mediaMaxID = Integer.parseInt(s.toString()) ;
			
			EditText text3=(EditText)findViewById(R.id.error) ;
			text3.setText(Integer.parseInt(s.toString())+" --- "+error) ;
		}
		
		protected void onProgressUpdate() {
			
		}
		}
}
